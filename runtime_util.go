// Package runtimeutil contains useful utilities
// that make use of the `runtime` package
package runtimeutil

import (
	"runtime"
	"strings"
)

func FuncOfCallerIndex(skip int) string {
	pc := make([]uintptr, skip+1)
	n := runtime.Callers(skip, pc)
	frames := runtime.CallersFrames(pc[:n])
	frame, _ := frames.Next()
	return frame.Function
}

func FuncNameOfCallerIndex(skip int) string {
	f := FuncOfCallerIndex(skip + 1)
	return f[strings.LastIndex(f, ".")+1:]
}

// CallerFunc returns the full function string including packages
// of the function that called the function calling CallerFunc()
func CallerFunc() string {
	// 1: FuncOfCallerIndex
	// 2: CallerFunc
	// 3: <Function calling this>
	// 4: <Caller function of function calling this>
	return FuncOfCallerIndex(4)
}

// ThisFunc returns the full function string including packages
// of the function calling ThisFunc()
func ThisFunc() string {
	// 1: FuncOfCallerIndex
	// 2: ThisFunc
	// 3: <Function calling this>
	return FuncOfCallerIndex(3)
}

// CallerFunc returns only the function name
// of the function that called the function calling CallerFunc()
func CallerFuncName() string {
	// 1: FuncNameOfCallerIndex
	// 2: CallerFuncName
	// 3: <Function calling this>
	// 4: <Caller function of function calling this>
	return FuncNameOfCallerIndex(4)
}

// ThisFunc returns only the function name
// of the function calling ThisFunc()
func ThisFuncName() string {
	// 1: FuncNameOfCallerIndex
	// 2: ThisFuncName
	// 3: <Function calling this>
	return FuncNameOfCallerIndex(3)
}
