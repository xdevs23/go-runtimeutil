package runtimeutil

import "testing"

func callerFunc() string {
	return CallerFunc()
}

func thisFunc() string {
	return ThisFunc()
}

func callerFuncName() string {
	return CallerFuncName()
}

func thisFuncName() string {
	return ThisFuncName()
}

func TestCallerFunc(t *testing.T) {
	s := callerFunc()
	expect := "gitlab.com/xdevs23/go-runtimeutil.TestCallerFunc"
	if s != expect {
		t.Fatalf("callerFunc() should return %s, instead returned: %s", expect, s)
	}
}

func TestThisFunc(t *testing.T) {
	s := thisFunc()
	expect := "gitlab.com/xdevs23/go-runtimeutil.thisFunc"
	if s != expect {
		t.Fatalf("thisFunc() should return %s, instead returned: %s", expect, s)
	}
}

func TestCallerFuncName(t *testing.T) {
	s := callerFuncName()
	if s != "TestCallerFuncName" {
		t.Fatal("callerFuncName() should return TestCallerFuncName, instead returned:", s)
	}
}

func TestThisFuncName(t *testing.T) {
	s := thisFuncName()
	if s != "thisFuncName" {
		t.Fatal("thisFuncName() should return thisFuncName, instead returned:", s)
	}
}
